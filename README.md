# Web-based visual analysis tool for criminal investigation
This tool forms a part of the research project __Analysis__ — “__Complex Analysis and Visualization of Large-scale Heterogeneous Data__”, whose goal is to provide a unified system for solving criminal investigations, allowing the police forces to access, analyze, visualize and collectively share relevant data or information. The frontend of the system is a __visual analysis tool__ with multiple highly-interactive data visualization modules to allow exploration, effective decision-making, and reasoning about the data, in a way that is intuitive and comprehensible to the analysts solving the crimes.

This is an implementation of the visual analysis tool. More detailed information about this tool, such as system design and requirements, can be found in Kristína Zákopčanová's thesis “[Visual Analysis of Data for Criminal Investigation](https://is.muni.cz/auth/th/ujzyx/)”. The follow-up work done by Marko Řeháček “[Building a web-based interactive network visualization in Vue.js](https://is.muni.cz/auth/th/lxum8/)” explains the selection of technologies, the system architecture, and algorithms for aggregating nodes in the node-link diagram.

_Project Analysis is supported by the Ministry of the Interior of the Czech Republic within the program “Security Research for the Needs of the State Program 2015–2020” under identification VI20172020096. The project is a cooperation between the Faculty of Informatics (Masaryk University), Institute of Computer Science – ÚVT (Masaryk University), and the Police of the Czech Republic._

See the [__latest build__](https://www.fi.muni.cz/~xrehacek/analysis-vis) of the tool.

## Technologies
The tool is built using the frontend framework [Vue.js](https://github.com/vuejs/vue) and the state management library [Vuex](https://github.com/vuejs/vuex). Data operations in store modules are tested using the testing framework [Jest](https://github.com/vuejs/vue). [Buefy](https://buefy.org/documentation/) is used for UI components. [D3-force](https://github.com/d3/d3-force) is used to calculate layout for the node-link diagram, and [D3-zoom](https://github.com/d3/d3-zoom) together with [D3-selection](https://github.com/d3/d3-selection) are used to setup zoom/pan behaviour.

## Project setup
1. Install the JavaScript runtime [Node.js](https://nodejs.org/en/), which also includes _NPM_, a package manager for handling dependencies.
2. Install [Python](https://www.python.org/downloads/), which is required to compile _node-sass_, one of the dependencies.
3. Using NPM, install project's dependecies:

```
npm install
```

4. Next, install tooling for Vue (`-g` for global install), which will take care of building the application:

```
npm install -g @vue/cli
```

5. To compile for development with hot-reloading and run locally, use:

```
npm run serve
```

6. _Optionally, to compile and minify for production, run:_

```
npm run build
```

## Development guides

<details>
<summary>Application's architecture</summary>

Diagram with high level overview of components and project structure:
[![Diagram with high level overview of components and project structure](docs/architecture-highlevel-preview.png "High-level overview of the applications architecture.")](docs/architecture-highlevel.png)

Simplified component tree:
[![Simplified diagram of components](docs/architecture-components-preview.png "Component tree.")](docs/architecture-components.png)

</details>

<details>
<summary>Documenting</summary>

Use [JSDoc](https://devdocs.io/jsdoc/) to document store modules (there is no support for Vue's __single file components__). Using proper tools, it allows [type checking as in TypeScript](https://medium.com/@trukrs/type-safe-javascript-with-jsdoc-7a2a63209b76). 

To enable type checking in VS Code, turn on this option in editor settings:
```
"javascript.implicitProjectConfig.checkJs": true
```

</details>

<details>
<summary>Testing</summary>

Using [Jest](https://jestjs.io/docs/en/getting-started). Tests are located in `/tests/unit/` and need to be named `*.spec.(js|jsx|ts|tsx)` – Jest will automatically load them.

Run all tests with:

```
npm run test
```

To additonally generate HTML coverage report in `/coverage`, run:

```
npm run test:coverage
```

</details>

<details>
<summary>Debugging in VS Code</summary>

Open debug tab, edit `launch.json`:

```
{
  "version": "0.2.0",
  "configurations": [
    {
      "type": "chrome",
      "request": "launch",
      "name": "Launch Chrome against localhost",
      "url": "http://localhost:8080",
      "webRoot": "${workspaceFolder}"
    },
    {
      "name": "jest-tests",
      "type": "node",
      "request": "launch",
      "runtimeArgs": [
        "--inspect-brk",
        "./node_modules/@vue/cli-service/bin/vue-cli-service.js",
        "test:unit",
        "--runInBand"
      ],
      "cwd": "${workspaceFolder}",
      "protocol": "inspector",
      "disableOptimisticBPs": true,
      "console": "integratedTerminal",
      "internalConsoleOptions": "neverOpen",
      "outFiles": ["${workspaceFolder}/src/**/*.js"],
      "port": 9229
    }
  ]
}
```

</details>

<details>
<summary>Extensions</summary>

Pretty much required:

- Vetur (Pine Wu)
- Prettier - Code formatter (Esben Peterson)
- ESLint (Dirk Baeumer)
- Sass (Robin Bentley)

Useful:

- Vue VSCode Snippets (Sarah Drasner)
- Vue Peek (Dario Fuzinato)
- Auto Close Tag, Auto Rename Tag (Jun Han)

</details>

<details>
<summary>Contribution</summary>

See [issue board](https://gitlab.fi.muni.cz/xrehacek/analysis-vis-prototype/-/boards/731).

Follow this git workflow: https://nvie.com/posts/a-successful-git-branching-model/.

</details>

## License
_TBD._

The design and development of this tool is a collaborative work of Jozef Bátrna, Barbora Kozlíková, Marko Řeháček, and Kristína Zákopčanová.