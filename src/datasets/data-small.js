export default {
  nodes: [
    {
      id: 0,
      name: 'Danielle Cowan',
      dataClass: 'person',
      type: 'regular',
      label: 'Danielle'
    },
    {
      id: 1,
      name: 'Malcolm Wang',
      dataClass: 'person',
      type: 'regular',
      label: 'Malcolm'
    },
    {
      id: 2,
      name: 'Paula Walter',
      dataClass: 'person',
      type: 'regular',
      label: 'Paula'
    },
    {
      id: 3,
      name: 'Steven Kang',
      dataClass: 'person',
      type: 'regular',
      label: 'Steven'
    },
    {
      id: 4,
      name: 'Alan Hardison',
      dataClass: 'person',
      type: 'regular',
      label: 'A. Hardison'
    }
  ],

  links: [
    {
      id: 0,
      source: 3,
      target: 1,
      direction: false,
      description: 'Norman'
    },
    {
      id: 1,
      source: 3,
      target: 0,
      direction: true,
      description: 'Walter'
    },
    {
      id: 2,
      source: 4,
      target: 3,
      direction: false,
      description: 'Lisa'
    },
    {
      id: 3,
      source: 2,
      target: 3,
      direction: false,
      description: 'Leah'
    },
    {
      id: 4,
      source: 0,
      target: 1,
      direction: true,
      description: 'Dean'
    },
    {
      id: 5,
      source: 0,
      target: 2,
      direction: true,
      description: 'Ethel'
    }
  ]
}
