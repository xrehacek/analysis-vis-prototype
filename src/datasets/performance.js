export default {
  nodes: [
    {
      id: 0,
      type: 'regular',
      name: 'Shannon Navarro',
      label: 'Debra',
      dataClass: 'Person'
    },
    {
      id: 1,
      type: 'regular',
      name: 'Dorothea Walls',
      label: 'Nadine',
      dataClass: 'Person'
    },
    {
      id: 2,
      type: 'regular',
      name: 'Miranda Holland',
      label: 'Pratt',
      dataClass: 'Person'
    },
    {
      id: 3,
      type: 'regular',
      name: 'Justice Rutledge',
      label: 'Dixon',
      dataClass: 'Person'
    },
    {
      id: 4,
      type: 'regular',
      name: 'Samantha Holt',
      label: 'Rachael',
      dataClass: 'Person'
    },
    {
      id: 5,
      type: 'regular',
      name: 'Meyers Farmer',
      label: 'Sharpe',
      dataClass: 'Person'
    },
    {
      id: 6,
      type: 'regular',
      name: 'Golden Mills',
      label: 'Lakeisha',
      dataClass: 'Person'
    },
    {
      id: 7,
      type: 'regular',
      name: 'Peck Anthony',
      label: 'Garza',
      dataClass: 'Person'
    },
    {
      id: 8,
      type: 'regular',
      name: 'Silva Price',
      label: 'Sonya',
      dataClass: 'Person'
    },
    {
      id: 9,
      type: 'regular',
      name: 'Maria Christensen',
      label: 'Fuller',
      dataClass: 'Person'
    },
    {
      id: 10,
      type: 'regular',
      name: 'Osborne Herman',
      label: 'Simone',
      dataClass: 'Person'
    },
    {
      id: 11,
      type: 'regular',
      name: 'Tricia Witt',
      label: 'Lydia',
      dataClass: 'Person'
    },
    {
      id: 12,
      type: 'regular',
      name: 'Moon Nelson',
      label: 'Wells',
      dataClass: 'Person'
    },
    {
      id: 13,
      type: 'regular',
      name: 'Aisha Nunez',
      label: 'Wallace',
      dataClass: 'Person'
    },
    {
      id: 14,
      type: 'regular',
      name: 'Benjamin Morris',
      label: 'Bush',
      dataClass: 'Person'
    },
    {
      id: 15,
      type: 'regular',
      name: 'Earnestine Mckenzie',
      label: 'Mills',
      dataClass: 'Person'
    },
    {
      id: 16,
      type: 'regular',
      name: 'Duke Hampton',
      label: 'Kathie',
      dataClass: 'Person'
    },
    {
      id: 17,
      type: 'regular',
      name: 'Gertrude Mcguire',
      label: 'Victoria',
      dataClass: 'Person'
    },
    {
      id: 18,
      type: 'regular',
      name: 'Patton Keller',
      label: 'Mays',
      dataClass: 'Person'
    },
    {
      id: 19,
      type: 'regular',
      name: 'Pauline Alston',
      label: 'Peterson',
      dataClass: 'Person'
    },
    {
      id: 20,
      type: 'regular',
      name: 'Oliver Parks',
      label: 'Tania',
      dataClass: 'Person'
    },
    {
      id: 21,
      type: 'regular',
      name: 'Rosa Cabrera',
      label: 'Barrera',
      dataClass: 'Person'
    },
    {
      id: 22,
      type: 'regular',
      name: 'Ila Grant',
      label: 'Villarreal',
      dataClass: 'Person'
    },
    {
      id: 23,
      type: 'regular',
      name: 'Morrow Matthews',
      label: 'Janette',
      dataClass: 'Person'
    },
    {
      id: 24,
      type: 'regular',
      name: 'Bridges Kennedy',
      label: 'Farley',
      dataClass: 'Person'
    },
    {
      id: 25,
      type: 'regular',
      name: 'Gay Walter',
      label: 'Ryan',
      dataClass: 'Person'
    },
    {
      id: 26,
      type: 'regular',
      name: 'Beryl Contreras',
      label: 'Richmond',
      dataClass: 'Person'
    },
    {
      id: 27,
      type: 'regular',
      name: 'Mable Fuentes',
      label: 'Mckinney',
      dataClass: 'Person'
    },
    {
      id: 28,
      type: 'regular',
      name: 'Mcbride Gates',
      label: 'Myra',
      dataClass: 'Person'
    },
    {
      id: 29,
      type: 'regular',
      name: 'Susan Wright',
      label: 'Craig',
      dataClass: 'Person'
    },
    {
      id: 30,
      type: 'regular',
      name: 'Maribel Gilbert',
      label: 'Natasha',
      dataClass: 'Person'
    },
    {
      id: 31,
      type: 'regular',
      name: 'Pamela Phillips',
      label: 'Staci',
      dataClass: 'Person'
    },
    {
      id: 32,
      type: 'regular',
      name: 'Maritza Harvey',
      label: 'Pope',
      dataClass: 'Person'
    },
    {
      id: 33,
      type: 'regular',
      name: 'Soto Bryant',
      label: 'Carlene',
      dataClass: 'Person'
    },
    {
      id: 34,
      type: 'regular',
      name: 'Evangeline Hendrix',
      label: 'Rowe',
      dataClass: 'Person'
    },
    {
      id: 35,
      type: 'regular',
      name: 'Hampton Carlson',
      label: 'Bryant',
      dataClass: 'Person'
    },
    {
      id: 36,
      type: 'regular',
      name: 'Joseph Harper',
      label: 'Latasha',
      dataClass: 'Person'
    },
    {
      id: 37,
      type: 'regular',
      name: 'Brennan Church',
      label: 'Marcia',
      dataClass: 'Person'
    },
    {
      id: 38,
      type: 'regular',
      name: 'Berry Carpenter',
      label: 'Matilda',
      dataClass: 'Person'
    },
    {
      id: 39,
      type: 'regular',
      name: 'Salinas Michael',
      label: 'Graciela',
      dataClass: 'Person'
    },
    {
      id: 40,
      type: 'regular',
      name: 'Annabelle Delaney',
      label: 'Pate',
      dataClass: 'Person'
    },
    {
      id: 41,
      type: 'regular',
      name: 'Eileen Watts',
      label: 'Booker',
      dataClass: 'Person'
    },
    {
      id: 42,
      type: 'regular',
      name: 'Valencia Booker',
      label: 'Contreras',
      dataClass: 'Person'
    },
    {
      id: 43,
      type: 'regular',
      name: 'Gillespie Burt',
      label: 'Oneil',
      dataClass: 'Person'
    },
    {
      id: 44,
      type: 'regular',
      name: 'Chan Lane',
      label: 'Dalton',
      dataClass: 'Person'
    },
    {
      id: 45,
      type: 'regular',
      name: 'Mia Cantrell',
      label: 'Graham',
      dataClass: 'Person'
    },
    {
      id: 46,
      type: 'regular',
      name: 'Burns Mendez',
      label: 'Huff',
      dataClass: 'Person'
    },
    {
      id: 47,
      type: 'regular',
      name: 'Levine Mayer',
      label: 'Valentine',
      dataClass: 'Person'
    },
    {
      id: 48,
      type: 'regular',
      name: 'Janna Berger',
      label: 'Austin',
      dataClass: 'Person'
    },
    {
      id: 49,
      type: 'regular',
      name: 'Isabel Hurley',
      label: 'Ollie',
      dataClass: 'Person'
    },
    {
      id: 50,
      type: 'regular',
      name: 'Neva Vazquez',
      label: 'Linda',
      dataClass: 'Person'
    },
    {
      id: 51,
      type: 'regular',
      name: 'Dena Conley',
      label: 'Wagner',
      dataClass: 'Person'
    },
    {
      id: 52,
      type: 'regular',
      name: 'Kelly Beard',
      label: 'Calhoun',
      dataClass: 'Person'
    },
    {
      id: 53,
      type: 'regular',
      name: 'Rich Preston',
      label: 'Claudine',
      dataClass: 'Person'
    },
    {
      id: 54,
      type: 'regular',
      name: 'Bettie Cole',
      label: 'Katherine',
      dataClass: 'Person'
    },
    {
      id: 55,
      type: 'regular',
      name: 'Diaz Castillo',
      label: 'Francisca',
      dataClass: 'Person'
    },
    {
      id: 56,
      type: 'regular',
      name: 'Wiggins Vang',
      label: 'Clare',
      dataClass: 'Person'
    },
    {
      id: 57,
      type: 'regular',
      name: 'Townsend Ware',
      label: 'Susana',
      dataClass: 'Person'
    },
    {
      id: 58,
      type: 'regular',
      name: 'Higgins Bowers',
      label: 'Jasmine',
      dataClass: 'Person'
    },
    {
      id: 59,
      type: 'regular',
      name: 'Juliette Snow',
      label: 'Stacey',
      dataClass: 'Person'
    },
    {
      id: 60,
      type: 'regular',
      name: 'Pollard Wade',
      label: 'Dawn',
      dataClass: 'Person'
    },
    {
      id: 61,
      type: 'regular',
      name: 'Nina Perez',
      label: 'Hurley',
      dataClass: 'Person'
    },
    {
      id: 62,
      type: 'regular',
      name: 'Terry Randolph',
      label: 'Eddie',
      dataClass: 'Person'
    },
    {
      id: 63,
      type: 'regular',
      name: 'Luella Ballard',
      label: 'Buck',
      dataClass: 'Person'
    },
    {
      id: 64,
      type: 'regular',
      name: 'Short Kane',
      label: 'Frazier',
      dataClass: 'Person'
    },
    {
      id: 65,
      type: 'regular',
      name: 'Lilia Walsh',
      label: 'Morton',
      dataClass: 'Person'
    },
    {
      id: 66,
      type: 'regular',
      name: 'Hood Blevins',
      label: 'Kristie',
      dataClass: 'Person'
    },
    {
      id: 67,
      type: 'regular',
      name: 'Cathy Justice',
      label: 'Jodi',
      dataClass: 'Person'
    },
    {
      id: 68,
      type: 'regular',
      name: 'Long Cooke',
      label: 'Joanna',
      dataClass: 'Person'
    },
    {
      id: 69,
      type: 'regular',
      name: 'Bobbi William',
      label: 'Pickett',
      dataClass: 'Person'
    },
    {
      id: 70,
      type: 'regular',
      name: 'Mitzi Conrad',
      label: 'Hazel',
      dataClass: 'Person'
    },
    {
      id: 71,
      type: 'regular',
      name: 'Rosetta Hines',
      label: 'Ayers',
      dataClass: 'Person'
    },
    {
      id: 72,
      type: 'regular',
      name: 'Boyd Mendoza',
      label: 'Minnie',
      dataClass: 'Person'
    },
    {
      id: 73,
      type: 'regular',
      name: 'King Potter',
      label: 'Nichols',
      dataClass: 'Person'
    },
    {
      id: 74,
      type: 'regular',
      name: 'Spears Langley',
      label: 'Richards',
      dataClass: 'Person'
    },
    {
      id: 75,
      type: 'regular',
      name: 'Reyna Mcknight',
      label: 'Madden',
      dataClass: 'Person'
    },
    {
      id: 76,
      type: 'regular',
      name: 'Mendoza Lynch',
      label: 'Newton',
      dataClass: 'Person'
    },
    {
      id: 77,
      type: 'regular',
      name: 'Nanette Benton',
      label: 'Finley',
      dataClass: 'Person'
    },
    {
      id: 78,
      type: 'regular',
      name: 'Dawson Cox',
      label: 'Ruiz',
      dataClass: 'Person'
    },
    {
      id: 79,
      type: 'regular',
      name: 'Ofelia Mcconnell',
      label: 'Polly',
      dataClass: 'Person'
    },
    {
      id: 80,
      type: 'regular',
      name: 'Miles Bennett',
      label: 'Ola',
      dataClass: 'Person'
    },
    {
      id: 81,
      type: 'regular',
      name: 'Parker Booth',
      label: 'Wheeler',
      dataClass: 'Person'
    },
    {
      id: 82,
      type: 'regular',
      name: 'Jenna Baxter',
      label: 'Rosa',
      dataClass: 'Person'
    },
    {
      id: 83,
      type: 'regular',
      name: 'Ursula Martin',
      label: 'Erma',
      dataClass: 'Person'
    },
    {
      id: 84,
      type: 'regular',
      name: 'Bowers Todd',
      label: 'Cole',
      dataClass: 'Person'
    },
    {
      id: 85,
      type: 'regular',
      name: 'Everett Burns',
      label: 'Whitley',
      dataClass: 'Person'
    },
    {
      id: 86,
      type: 'regular',
      name: 'Wynn Richards',
      label: 'Gladys',
      dataClass: 'Person'
    },
    {
      id: 87,
      type: 'regular',
      name: 'Wong Barry',
      label: 'Hicks',
      dataClass: 'Person'
    },
    {
      id: 88,
      type: 'regular',
      name: 'Moody Calderon',
      label: 'Kristin',
      dataClass: 'Person'
    },
    {
      id: 89,
      type: 'regular',
      name: 'Estela Campos',
      label: 'Trevino',
      dataClass: 'Person'
    },
    {
      id: 90,
      type: 'regular',
      name: 'Keith Taylor',
      label: 'Schneider',
      dataClass: 'Person'
    },
    {
      id: 91,
      type: 'regular',
      name: 'Lynette Jarvis',
      label: 'Montoya',
      dataClass: 'Person'
    },
    {
      id: 92,
      type: 'regular',
      name: 'Kelley Austin',
      label: 'Sara',
      dataClass: 'Person'
    },
    {
      id: 93,
      type: 'regular',
      name: 'Clarke Pena',
      label: 'Lilian',
      dataClass: 'Person'
    },
    {
      id: 94,
      type: 'regular',
      name: 'Davis Griffith',
      label: 'Byrd',
      dataClass: 'Person'
    },
    {
      id: 95,
      type: 'regular',
      name: 'Claudette Nolan',
      label: 'Lakisha',
      dataClass: 'Person'
    },
    {
      id: 96,
      type: 'regular',
      name: 'Case Snyder',
      label: 'Ramirez',
      dataClass: 'Person'
    },
    {
      id: 97,
      type: 'regular',
      name: 'Mercedes Whitley',
      label: 'Zamora',
      dataClass: 'Person'
    },
    {
      id: 98,
      type: 'regular',
      name: 'Dillard Chase',
      label: 'Burgess',
      dataClass: 'Person'
    },
    {
      id: 99,
      type: 'regular',
      name: 'Myers Miranda',
      label: 'Shirley',
      dataClass: 'Person'
    }
  ],

  // LINKS

  links: [
    {
      id: 0,
      source: 0,
      target: 2
    },
    {
      id: 1,
      source: 1,
      target: 3
    },
    {
      id: 2,
      source: 2,
      target: 4
    },
    {
      id: 3,
      source: 3,
      target: 5
    },
    {
      id: 4,
      source: 4,
      target: 6
    },
    {
      id: 5,
      source: 5,
      target: 7
    },
    {
      id: 6,
      source: 6,
      target: 8
    },
    {
      id: 7,
      source: 7,
      target: 9
    },
    {
      id: 8,
      source: 8,
      target: 10
    },
    {
      id: 9,
      source: 9,
      target: 11
    },
    {
      id: 10,
      source: 10,
      target: 12
    },
    {
      id: 11,
      source: 11,
      target: 13
    },
    {
      id: 12,
      source: 12,
      target: 14
    },
    {
      id: 13,
      source: 13,
      target: 15
    },
    {
      id: 14,
      source: 14,
      target: 16
    },
    {
      id: 15,
      source: 15,
      target: 17
    },
    {
      id: 16,
      source: 16,
      target: 18
    },
    {
      id: 17,
      source: 17,
      target: 19
    },
    {
      id: 18,
      source: 18,
      target: 20
    },
    {
      id: 19,
      source: 19,
      target: 21
    },
    {
      id: 20,
      source: 20,
      target: 22
    },
    {
      id: 21,
      source: 21,
      target: 23
    },
    {
      id: 22,
      source: 22,
      target: 24
    },
    {
      id: 23,
      source: 23,
      target: 25
    },
    {
      id: 24,
      source: 24,
      target: 26
    },
    {
      id: 25,
      source: 25,
      target: 27
    },
    {
      id: 26,
      source: 26,
      target: 28
    },
    {
      id: 27,
      source: 27,
      target: 29
    },
    {
      id: 28,
      source: 28,
      target: 30
    },
    {
      id: 29,
      source: 29,
      target: 31
    },
    {
      id: 30,
      source: 30,
      target: 32
    },
    {
      id: 31,
      source: 31,
      target: 33
    },
    {
      id: 32,
      source: 32,
      target: 34
    },
    {
      id: 33,
      source: 33,
      target: 35
    },
    {
      id: 34,
      source: 34,
      target: 36
    },
    {
      id: 35,
      source: 35,
      target: 37
    },
    {
      id: 36,
      source: 36,
      target: 38
    },
    {
      id: 37,
      source: 37,
      target: 39
    },
    {
      id: 38,
      source: 38,
      target: 40
    },
    {
      id: 39,
      source: 39,
      target: 41
    },
    {
      id: 40,
      source: 40,
      target: 42
    },
    {
      id: 41,
      source: 41,
      target: 43
    },
    {
      id: 42,
      source: 42,
      target: 44
    },
    {
      id: 43,
      source: 43,
      target: 45
    },
    {
      id: 44,
      source: 44,
      target: 46
    },
    {
      id: 45,
      source: 45,
      target: 47
    },
    {
      id: 46,
      source: 46,
      target: 48
    },
    {
      id: 47,
      source: 47,
      target: 49
    },
    {
      id: 48,
      source: 48,
      target: 50
    },
    {
      id: 49,
      source: 49,
      target: 51
    },
    {
      id: 50,
      source: 50,
      target: 52
    },
    {
      id: 51,
      source: 51,
      target: 53
    },
    {
      id: 52,
      source: 52,
      target: 54
    },
    {
      id: 53,
      source: 53,
      target: 55
    },
    {
      id: 54,
      source: 54,
      target: 56
    },
    {
      id: 55,
      source: 55,
      target: 57
    },
    {
      id: 56,
      source: 56,
      target: 58
    },
    {
      id: 57,
      source: 57,
      target: 59
    },
    {
      id: 58,
      source: 58,
      target: 60
    },
    {
      id: 59,
      source: 59,
      target: 61
    },
    {
      id: 60,
      source: 60,
      target: 62
    },
    {
      id: 61,
      source: 61,
      target: 63
    },
    {
      id: 62,
      source: 62,
      target: 64
    },
    {
      id: 63,
      source: 63,
      target: 65
    },
    {
      id: 64,
      source: 64,
      target: 66
    },
    {
      id: 65,
      source: 65,
      target: 67
    },
    {
      id: 66,
      source: 66,
      target: 68
    },
    {
      id: 67,
      source: 67,
      target: 69
    },
    {
      id: 68,
      source: 68,
      target: 70
    },
    {
      id: 69,
      source: 69,
      target: 71
    },
    {
      id: 70,
      source: 70,
      target: 72
    },
    {
      id: 71,
      source: 71,
      target: 73
    },
    {
      id: 72,
      source: 72,
      target: 74
    },
    {
      id: 73,
      source: 73,
      target: 75
    },
    {
      id: 74,
      source: 74,
      target: 76
    },
    {
      id: 75,
      source: 75,
      target: 77
    },
    {
      id: 76,
      source: 76,
      target: 78
    },
    {
      id: 77,
      source: 77,
      target: 79
    },
    {
      id: 78,
      source: 78,
      target: 80
    },
    {
      id: 79,
      source: 79,
      target: 81
    },
    {
      id: 80,
      source: 80,
      target: 82
    },
    {
      id: 81,
      source: 81,
      target: 83
    },
    {
      id: 82,
      source: 82,
      target: 84
    },
    {
      id: 83,
      source: 83,
      target: 85
    },
    {
      id: 84,
      source: 84,
      target: 86
    },
    {
      id: 85,
      source: 85,
      target: 87
    },
    {
      id: 86,
      source: 86,
      target: 88
    },
    {
      id: 87,
      source: 87,
      target: 89
    },
    {
      id: 88,
      source: 88,
      target: 90
    },
    {
      id: 89,
      source: 89,
      target: 91
    },
    {
      id: 90,
      source: 90,
      target: 92
    },
    {
      id: 91,
      source: 91,
      target: 93
    },
    {
      id: 92,
      source: 92,
      target: 94
    },
    {
      id: 93,
      source: 93,
      target: 95
    },
    {
      id: 94,
      source: 94,
      target: 96
    },
    {
      id: 95,
      source: 95,
      target: 97
    },
    {
      id: 96,
      source: 96,
      target: 98
    },
    {
      id: 97,
      source: 97,
      target: 99
    }
  ]
}
