export const namespaced = true

export const state = {
  /**
   * @type {number[]} node/link IDs
   */
  selectedNodes: [],
  highlightedNodes: [],
  highlightedLinks: [],
  /**
   * @type {number} node/link ID
   */
  detailedNode: NaN,
  hoveredNode: NaN,
  detailedLink: NaN,

  shownTools: {
    nodeDetail: false,
    nodePopover: false,
    nodeSelection: false,
    linkDetail: false,
    datasetEditor: false
  },
  inDebug: process.env.NODE_ENV !== 'production'
}

export const mutations = {
  SET_NODE_HIGHLIGHT(state, id) {
    state.highlightedNodes.push(id)
  },
  UNSET_NODE_HIGHLIGHT(state, id) {
    state.highlightedNodes = state.highlightedNodes.filter(n => n !== id)
  },
  CLEAR_NODE_HIGHLIGHT(state) {
    state.highlightedNodes = []
  },
  SET_NODE_DETAIL(state, id) {
    state.detailedNode = id
  },
  UNSET_NODE_DETAIL(state) {
    state.detailedNode = NaN
  },
  SET_NODE_HOVER(state, id) {
    state.hoveredNode = id
  },
  UNSET_NODE_HOVER(state) {
    state.hoveredNode = NaN
  },
  SELECT_NODE(state, id) {
    state.selectedNodes.push(id)
  },
  DESELECT_NODES(state, idArr) {
    state.selectedNodes = state.selectedNodes.filter(id => !idArr.includes(id))
  },
  CLEAR_NODE_SELECTION(state) {
    state.selectedNodes = []
  },

  SET_LINK_HIGHLIGHT(state, id) {
    state.highlightedLinks.push(id)
  },
  CLEAR_LINK_HIGHLIGHT(state) {
    state.highlightedLinks = []
  },
  SHOW_LINK_DETAIL(state, id) {
    state.detailedLink = id
  },
  HIDE_LINK_DETAIL(state) {
    state.detailedLink = NaN
    state.shownTools.linkDetail = false
  },

  SET_TOOL_SHOW(state, { tool, bool }) {
    state.shownTools[tool] = bool
  },

  SET_DEBUG(state, bool) {
    state.inDebug = bool
  }
}

export const actions = {
  showNodeDetail({ commit }) {
    commit('SET_TOOL_SHOW', { tool: 'nodeDetail', bool: true })
  },
  hideNodeDetail({ commit }) {
    commit('SET_TOOL_SHOW', { tool: 'nodeDetail', bool: false })
    commit('UNSET_NODE_DETAIL')
  },

  showNodePopover({ commit }, id) {
    commit('SET_NODE_HOVER', id)
    commit('SET_TOOL_SHOW', { tool: 'nodePopover', bool: true })
  },
  hideNodePopover({ commit }) {
    commit('SET_TOOL_SHOW', { tool: 'nodePopover', bool: false })
    commit('UNSET_NODE_HOVER')
  },

  showNodeSelection({ commit }) {
    commit('SET_TOOL_SHOW', { tool: 'nodeSelection', bool: true })
  },
  clearCloseNodeSelection({ commit }) {
    commit('SET_TOOL_SHOW', { tool: 'nodeSelection', bool: false })
    commit('CLEAR_NODE_SELECTION')
  },
  hideNodeSelectionIfEmpty({ commit }) {
    if (state.selectedNodes.length !== 0) return
    commit('SET_TOOL_SHOW', { tool: 'nodeSelection', bool: false })
  },
  deselectNode({ commit, dispatch }, id) {
    commit('DESELECT_NODES', [id])
    dispatch('hideNodeSelectionIfEmpty')
  },

  showLinkDetail({ commit }, id) {
    commit('SHOW_LINK_DETAIL', id) 
    commit('SET_TOOL_SHOW', { tool: 'linkDetail', bool: true })
  },
  hideLinkDetail({ commit }) {
    commit('SET_TOOL_SHOW', { tool: 'linkDetail', bool: false })
    commit('HIDE_LINK_DETAIL')
  },

  showDatasetEditor({ commit }) {
    commit('SET_TOOL_SHOW', { tool: 'datasetEditor', bool: true })
  },
  hideDatasetEditor({ commit }) {
    commit('SET_TOOL_SHOW', { tool: 'datasetEditor', bool: false })
  },

  activateDebug({ commit }) {
    commit('SET_DEBUG', true)
  },
  deactivateDebug({ commit }) {
    commit('SET_DEBUG', false)
  },

  respondToRemovedNodes({ commit, state, dispatch }, removedNodes) {
    commit('DESELECT_NODES', removedNodes)
    if (removedNodes.includes(state.detailedNode)) dispatch('hideNodeDetail')
    if (removedNodes.includes(state.hoveredNode)) dispatch('hideNodeDetail')
    commit('HIDE_LINK_DETAIL')
  },

  stopHighlighting({ commit }) {
    commit('CLEAR_NODE_HIGHLIGHT')
    commit('CLEAR_LINK_HIGHLIGHT')
  }
}

export const getters = {
  isShownNodeDetail: state => state.shownTools['nodeDetail'],
  isShownNodePopover: state => state.shownTools['nodePopover'],
  isShownNodeSelection: state => state.shownTools['nodeSelection'],
  isShownLinkDetail: state => state.shownTools['linkDetail'],
  isShownDatasetEditor: state => state.shownTools['datasetEditor'],

  isNodeHovered: state => id => state.hoveredNode === id,
  isNodeDetailed: state => id => state.detailedNode === id,
  isNodeSelected: state => id => state.selectedNodes.find(n => n === id) !== undefined,
  isNodeHighlighted: state => id => state.highlightedNodes.find(n => n === id) !== undefined,

  isLinkDetailed: state => id => state.detailedLink === id,
  isLinkHighlighted: state => id => state.highlightedLinks.find(n => n === id) !== undefined
}
