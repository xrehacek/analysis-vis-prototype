module.exports = {	
  publicPath:
    process.env.NODE_ENV === 'production'
      ? '/~xrehacek/analysis-vis/' + process.env.CI_COMMIT_REF_NAME
      : '/'
}
